import { makeObservable,makeAutoObservable, observable, computed, action } from 'mobx';
export default class VehicleModel {
    id: string;
    vehicleType: string;
    vehicleModel: string;
    pricing: string
    fuelType: string
    seats: string
    doors: string
    fuelConsumption: string
    gearType: string
    bodyType: string
    engineType: string
    pricingPerHour: string
    orderStatus: boolean


    constructor(id: string, vehicleType: string,vehicleModel: string,pricing: string,fuelType: string,seats: string,doors: string,fuelConsumption: string,gearType: string,bodyType: string,engineType: string,pricingPerHour: string,orderStatus:boolean) 
    {

        makeAutoObservable(this);
        this.id = id
        this.vehicleType = vehicleType
        this.vehicleModel = vehicleModel
        this.pricing = pricing
        this.fuelType = fuelType
        this.seats = seats
        this.doors = doors
        this.fuelConsumption = fuelConsumption
        this.gearType= gearType
        this.bodyType=bodyType
        this.engineType=engineType
        this.pricingPerHour= pricingPerHour
        this.orderStatus=orderStatus
    }

}