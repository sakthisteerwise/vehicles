import { makeObservable,makeAutoObservable, observable, computed, action } from 'mobx';
export default class LocationeModel {
    id: string;
    location: string;
    constructor(id: string, location: string) 
    {
        makeAutoObservable(this);
        this.id = id
        this.location = location
    }

}