import VehicleModel from '../../../model/vehicle.model'
export interface  VehicleSearchViewControllerInterface{
    location:string[];
    vehicleType:string[];
    getLocation():string[];
    getVehicleType():string[];
    isValidDate(date:string):boolean
    isValidPickingUpDate(startDate:Date,endDate:Date):boolean;
    isValiddroppingOffDate(startDate:Date,endDate:Date):boolean;
}


