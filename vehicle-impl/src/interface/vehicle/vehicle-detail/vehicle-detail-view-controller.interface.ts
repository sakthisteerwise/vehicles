import VehicleModel from '../../../model/vehicle.model'
export interface  VehicleDetailViewControllerInterface{
    vehicle : VehicleModel [] ;
    getVehicleDetail(vehicleId:Number):VehicleModel
}


