import VehicleModel from '../../../model/vehicle.model'
// Interface for Vehicle Search View Model
export interface  VehicleDetailViewModelInterface{
    vehicle : VehicleModel[] ;
    getVehicleDetail(vehicleId:Number):VehicleModel
}

