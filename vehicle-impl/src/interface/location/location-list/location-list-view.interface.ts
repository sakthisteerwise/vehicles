import LocationeModel from '../../../model/location.model'
// Interface for Vehicle List View Model
export interface  LocationListViewInterface{
    addLocation(locationId:Number):void
    editLocation(locationId:Number):void
    deleteLocation(locationId:Number):void
}
