import LocationeModel from '../../../model/location.model'
// Interface for Vehicle List View Model
export interface  LocationListViewControllerInterface{
    location:LocationeModel[];
    addLocation(location:string):void
    editLocation(locationId:Number,location:string):void
}
