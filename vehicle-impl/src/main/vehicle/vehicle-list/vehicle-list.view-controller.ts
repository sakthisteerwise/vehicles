import { makeObservable, observable, computed, action,flow } from 'mobx';
import { VehicleListViewControllerInterface } from '../../../interface/vehicle/vehicle-list/vehicle-list-view-controller.interface';
import VehicleModel from '../../../model/vehicle.model';

export class VehicleListViewController implements VehicleListViewControllerInterface {
    vehicle: VehicleModel[] = [];
    constructor() {
        makeObservable(this, {
            vehicle: observable,
        })
    }
    getVehicle(pickingUplocation: string, droppingOffLocation: string, vehicleModel: string, pickingUpDate: string, droppingOffDate: string): VehicleModel[] {
        throw new Error('Method not implemented.');
    }
    placeOrder(vehicleId: Number, pickingUplocation: string, droppingOffLocation: string, vehicleModel: string, pickingUpDate: string, droppingOffDate: string): string {
        throw new Error('Method not implemented.');
    }
    showVehicleDetails(vehicleId: Number): void {
        throw new Error('Method not implemented.');
    }
 
}