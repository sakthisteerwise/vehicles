import VehicleModel from '../../model/vehicle.model'
// Interface for Vehicle List View Model
export interface  VehicleListViewModelInterface{
    vehicle : VehicleModel[] ;
    getVehicle(pickingUplocation:string,droppingOffLocation:string,vehicleModel:string,pickingUpDate:string,droppingOffDate:string):VehicleModel[];
    getVehicleDetails(vehicleId:Number):VehicleModel;
    placeOrder(vehicleId:Number,pickingUplocation:string,droppingOffLocation:string,vehicleModel:string,pickingUpDate:string,droppingOffDate:string):string
}

// Interface for Vehicle List View Controller
export interface  VehicleListViewControllerInterface{
    vehicle : VehicleModel[] ;
    getVehicle(pickingUplocation:string,droppingOffLocation:string,vehicleModel:string,pickingUpDate:string,droppingOffDate:string):VehicleModel[];
    placeOrder():void;
    
}
// Interface for Vehicle Search View Model
export interface  VehicleSearchViewModelInterface{
    vehicle : VehicleModel[] ;
    location:string[];
    carType:string[];
    getVehicle(pickingUplocation:string,droppingOffLocation:string,vehicleModel:string,pickingUpDate:string,droppingOffDate:string):VehicleModel[];
    getLocation():string[];
    getVehicleType():string[];
    isValidDate(date:string):boolean
    isValidPickingUpDate(startDate:Date,endDate:Date):boolean;
    isValiddroppingOffDate(startDate:Date,endDate:Date):boolean;
}

export interface  VehicleServiceInterface{
    vehicle : VehicleModel[] ;
    location:string[];
    getVehicle():VehicleModel[];
    getVehicleDetails():VehicleModel;
    filterVehicle(pickingUplocation:string,droppingOffLocation:string,vehicleModel:string,pickingUpDate:string,droppingOffDate:string):VehicleModel[];
    getLocation():string[];
    getVehicleType():VehicleModel[];
    addLocation():string;
    editLocation():string;
    getOrder():string[]
    getOrderDetails():object
    placeOrder():string;
    editOrder():string;


}

export interface  VehicleSearchViewControllerInterface{
    vehicle : VehicleModel[] ;
    location:string[];
    carType:string[];
    filterVehicle(): VehicleModel[];

    getLocation():string[];

}
// Interface for Vehicle View View Model
export interface  VehicleViewViewModelInterface{
    vehicle : VehicleModel[] ;
    getVehicle():VehicleModel;   
}
export interface  VehicleViewViewControllerInterface{

}