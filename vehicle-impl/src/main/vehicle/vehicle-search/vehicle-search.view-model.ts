import { makeObservable, observable, computed, action,flow } from 'mobx';
import VehicleService from '../vehicle.service'
import VehicleModel from '../../../model/vehicle.model';
import VehicleSearchViewModelInterface from '../../../interface/vehicle/vehicle-search/vehicle-search-view-model.interface'

class VehicleSearchViewModel implements VehicleSearchViewModelInterface {
    vehicle: VehicleModel[] = [];
    
    constructor(private vehicleService: any) {
        makeObservable(this, {
            vehicle: observable,
        })
        
    }
    vehicleType: string[]=[];
    getLocation(): string[] {
        throw new Error('Method not implemented.');
    }
    getVehicleType(): string[] {
        throw new Error('Method not implemented.');
    }
    isValidDate(date: string): boolean {
        throw new Error('Method not implemented.');
    }
    isValidPickingUpDate(startDate: Date, endDate: Date): boolean {
        throw new Error('Method not implemented.');
    }
    isValiddroppingOffDate(startDate: Date, endDate: Date): boolean {
        throw new Error('Method not implemented.');
    }
    location: string[]=[];
    carType: string[]=[];
   
    
    
}

export default VehicleSearchViewModel;
