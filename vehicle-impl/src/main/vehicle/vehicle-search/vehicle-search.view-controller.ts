import { makeObservable, observable, computed, action,flow } from 'mobx';
import VehicleModel from '../../../model/vehicle.model';
import {VehicleSearchViewControllerInterface} from '../../../interface/vehicle/vehicle-search/vehicle-search-view-controller.interface'

export class VehicleSearchViewController implements VehicleSearchViewControllerInterface {
    vehicle: VehicleModel[] = [];
    constructor() {
        makeObservable(this, {
            vehicle: observable,
        })
    }
    location: string[]=[];
    vehicleType: string[]=[];
    getLocation(): string[] {
        throw new Error('Method not implemented.');
    }
    getVehicleType(): string[] {
        throw new Error('Method not implemented.');
    }
    isValidDate(date: string): boolean {
        throw new Error('Method not implemented.');
    }
    isValidPickingUpDate(startDate: Date, endDate: Date): boolean {
        throw new Error('Method not implemented.');
    }
    isValiddroppingOffDate(startDate: Date, endDate: Date): boolean {
        throw new Error('Method not implemented.');
    }
    
    
    
}