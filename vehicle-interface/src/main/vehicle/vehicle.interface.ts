import VehicleModel from './vehicle'
// Interface for Vehicle List View Model
export interface  VehicleListViewModelInterface{
    vehicles : VehicleModel[] ;
    loadVehicles():VehicleModel[];
    getVehicles():VehicleModel[];
}
// Interface for Vehicle List View Controller
export interface  VehicleListViewControllerInterface{
    vehicles : VehicleModel[] ;
    loadVehicles():VehicleModel[];
    getVehicles():VehicleModel[]; 
    reserveVehicle():void;
    
}
// Interface for Vehicle Search View Model
export interface  VehicleSearchViewModelInterface{
    vehicles : VehicleModel[] ;
    locations:string[];
    carType:string[];
    filterVehicles(pickingUplocation:string,droppingOffLocation:string,vehicleModel:string,pickingUpDate:string,droppingOffDate:string):VehicleModel[];
    getLocationss():string[];
    getCarType():string[];
}

export interface  VehicleSearchViewControllerInterface{
    vehicles : VehicleModel[] ;
    locations:string[];
    carType:string[];
    filterVehicles(): VehicleModel[];
    isValidPickingUpDate():void;
    isValiddroppingOffDate():void;
    getLocations():string[];

}
// Interface for Vehicle View View Model
export interface  VehicleViewViewModelInterface{
    vehicles : VehicleModel[] ;
    getVehicle():VehicleModel;   
}
export interface  VehicleViewViewControllerInterface{

}