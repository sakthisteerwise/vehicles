import { makeObservable,makeAutoObservable, observable, computed, action } from 'mobx';
export default class VehicleModel {
    id: string;
    vehicleType: string;
    vehicleModel: string;
    pricing: string
    fuelType: string
    seats: string
    doors: string
    fuelConsumption: string
    gearType: string
    bodyType: string
    engineType: string
    pricingPerHour: string


    constructor(id: string, vehicleType: string,vehicleModel: string,pricing: string,fuelType: string,seats: string,doors: string,fuelConsumption: string,gearType: string,bodyType: string,engineType: string,pricingPerHour: string) 
    {
        // makeObservable(this, {
        //     id: observable,
        //     vehicleType: observable,
        //     vehicleModel: observable,
        //     pricing: observable
        // })
        makeAutoObservable(this);
        this.id = id
        this.vehicleType = vehicleType
        this.vehicleModel = vehicleModel
        this.pricing = pricing
        this.fuelType = fuelType
        this.seats = seats
        this.doors = doors
        this.fuelConsumption = fuelConsumption
        this.gearType= gearType
        this.bodyType=bodyType
        this.engineType=engineType
        this.pricingPerHour= pricingPerHour
    }

    getId() {
        return this.id
    }
    getVehicleType() {
        return this.vehicleType
    }
    getVehicleModel() {
        return this.vehicleModel
    }
    getPricing() {
        return this.pricing
    }
}